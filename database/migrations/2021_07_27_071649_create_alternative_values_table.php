<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlternativeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternative_values', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('alternative_id')->unsigned();
            $table->bigInteger('criteria_id')->unsigned();
            $table->bigInteger('sub_criteria_id')->unsigned();
            $table->timestamps();

            $table->foreign('alternative_id')->references('id')->on('alternatives')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('criteria_id')->references('id')->on('criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_criteria_id')->references('id')->on('sub_criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternative_values');
    }
}
