<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriteriaValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria_values', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('criteria_1_id')->unsigned();
            $table->float('value');
            $table->bigInteger('criteria_2_id')->unsigned();
            $table->timestamps();

            $table->foreign('criteria_1_id')->references('id')->on('criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('criteria_2_id')->references('id')->on('criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria_values');
    }
}
