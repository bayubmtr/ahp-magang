<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCriteriaValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_criteria_values', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sub_criteria_1_id')->unsigned();
            $table->float('value');
            $table->bigInteger('sub_criteria_2_id')->unsigned();
            $table->timestamps();

            $table->foreign('sub_criteria_1_id')->references('id')->on('sub_criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_criteria_2_id')->references('id')->on('sub_criterias')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_criteria_values');
    }
}
