@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h2 class="m-0 text-dark">PENGUMUMAN HASIL</h2>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Pengumuman Hasil</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        @php
                        @endphp
                        <th width="10">NO</th>
                        <th>JUDUL</th>
                        <th>STATUS</th>
                        <th width="40">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $sis)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$sis->internship->name}}</td>
                        <td>
                            @if (!$sis->internship->announce_at)
                             <span class="badge badge-warning">MENUNGGU</span> 
                            @elseif($sis->status == 'DITERIMA')
                             <span class="badge badge-success">DITERIMA</span> 
                            @elseif($sis->status == 'DITOLAK')
                             <span class="badge badge-danger">DITOLAK</span> 
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($sis->internship->announce_at)
                            <a href="{{ route('results.show', $sis->internship_id) }}">
                                <button class="btn btn-info">Detail</button>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
