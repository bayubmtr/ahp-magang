@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">TAMBAH DATA MAGANG</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Tambah Data Magang</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card p-4">
        <div class="mb-4">
            <h3>{{ $internshipVacancy->name }}</h3>
        </div>
        <div>
            {!! $internshipVacancy->description !!}
        </div>
        @if(!$internshipVacancy->hasRegister)
        <div>
            <a href="{{ route('internship-vacancies.register', $internshipVacancy->id) }}" class="btn btn-danger">Lamar</a>
        </div>
        @endif
    </div>
</section>
@include ('includes.script')
@endsection
