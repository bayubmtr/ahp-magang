@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h2 class="m-0 text-dark">DAFTAR LOWONGAN MAGANG</h2>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Daftar Lowongan Magang</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="10">NO</th>
                        <th>JUDUL</th>
                        <th>PENDAFTAR</th>
                        <th width="180">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $sis)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$sis->name}}</td>
                        <td>{{$sis->registrants_count}}</td>
                        <td class="text-center">
                            <a href="{{ route('internship-vacancies.show', $sis->id) }}">
                                <button class="btn btn-info">Detail</button>
                            </a>
                            @if(!$sis->has_register_count)
                            <a href="{{ route('internship-vacancies.register', $sis->id) }}">
                                <button class="btn btn-danger">Lamar</button>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
