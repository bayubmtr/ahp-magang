@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">FORMULIR PENDAFTARAN MAGANG</h2>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                @include ('includes.flash')
                <form role="form" method="post" action="{{ route('internship-vacancies.register.store', $internshipVacancy->id) }}">
                    @csrf
                    <div class="card-body">
                        <div class="alert alert-danger" role="alert">
                        Isi data dibawah ini dengan data yang jujur.
                        </div>
                        @foreach($internshipVacancy->formula->criterias as $krit)
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{$krit->name}}</label>
                            <select class="form-control" name="value[{{$krit->id}}]">
                                @foreach($krit->subCriterias as $sub)
                                    <option value="{{$sub->id}}">{{$sub->name}} ( {{$sub->parameter}} )</option>
                                @endforeach
                            </select>
                        </div>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn mb-2 btn-primary float-right">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
