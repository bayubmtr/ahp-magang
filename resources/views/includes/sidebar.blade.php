<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SPK <sup>MAGANG</sup></div>
    </a>

    <hr class="sidebar-divider">

    @if(auth()->user()->role == 'admin')
    <div class="sidebar-heading">
        Master Data
    </div>

    <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('users.index') }}">
            <i class="fa fa-user"></i>
            <span>USER</span>
        </a>
    </li>

    <li class="nav-item {{ Request::is('internships') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('internships.index') }}">
            <i class="fa fa-list"></i>
            <span>MAGANG</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Perumusan
    </div>

    <li class="nav-item {{ Request::is('formulas*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('formulas.index') }}">
            <i class="fa fa-balance-scale"></i>
            <span>FORMULA</span>
        </a>
    </li>

    @else
    <li class="nav-item {{ Request::is('internship-vacancies') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('internship-vacancies.index') }}">
            <i class="fa fa-list"></i>
            <span>LOWONGAN MAGANG</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('results') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('results.index') }}">
            <i class="fa fa-bullhorn"></i>
            <span>PENGUMUMAN HASIL</span>
        </a>
    </li>
    @endif

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
