@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">KRITERIA</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Kriteria</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    <th width="40">NO</th>
                    <th>NAMA</th>
                    <th width="400">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $krit)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $krit->name }}</td>
                        <td class="text-center">
                            <a href="{{ route('formulas.criterias.sub-criterias.index', ['formula' => Request::route('formula'), 'criteria' => $krit->id]) }}">
                                <button class="btn btn-secondary">Daftar Sub Kriteria</button>
                            </a>
                            <a href="{{ route('formulas.criterias.sub-criteria-values.index', ['formula' => Request::route('formula'), 'criteria' => $krit->id]) }}">
                                <button class="btn btn-info">Bobot Sub Kriteria</button>
                            </a>
                            <a href="{{ route('criterias.edit', $krit->id) }}">
                            <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-pencil"></i></button>
                            </a>
                            <form id="delete-kriteria-{{ $krit->id }}" action="{{ route('criterias.destroy', $krit->id) }}" method="post" style="display: inline;">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
<script type="text/javascript">
$(document).ready(function(){
    $("#data-admin_length").append('<a  href="{{ route('formulas.criterias.create', Request::route('formula')) }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
});
</script>
@endsection
