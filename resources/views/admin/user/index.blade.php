@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h2 class="m-0 text-dark">USER</h2>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">User</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="10">NO</th>
                        <th>NAMA</th>
                        <th>EMAIL</th>
                        <th width="70">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $sis)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$sis->name}}</td>
                        <td>{{$sis->email}}</td>
                        <td class="text-center">
                            {{-- <a href="{{ route('users.edit', $sis->id) }}">
                                <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-pencil"></i></button>
                            </a> --}}
                            <form id="delete-siseria-{{$sis->id}}" action="{{ route('users.destroy', $sis->id) }}" method="post" style="display: inline;">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
