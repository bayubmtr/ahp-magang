@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">EDIT DATA MAGANG</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Edit Data Magang</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('internships.update', $internship->id) }}">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama/ Judul Magang</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name', $internship->name) }}" id="1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Formula</label>
                        <select @if ($internship->announce_at)
                            disabled
                        @endif name="formula_id" class="form-control">
                            <option value="">Pilih Formula</option>
                            @foreach ($formulas as $item)
                                <option @if(old('formula_id', $internship->formula_id) == $item->id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($internship->announce_at)
                        <small id="emailHelp" class="form-text text-muted">Tidak bisa diubah karena data sudah diumumkan</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Deskripsi</label>
                        <textarea name="description" id="editor">
                            {{ old('description', $internship->description) }}
                        </textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
<script src="https://cdn.ckeditor.com/ckeditor5/29.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
@endsection
