@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h2 class="m-0 text-dark">DATA LOWONGAN MAGANG</h2>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">User</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="10">NO</th>
                        <th>JUDUL</th>
                        <th>FORMULA</th>
                        <th>PENDAFTAR</th>
                        <th>STATUS</th>
                        <th width="300">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $sis)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$sis->name}}</td>
                        <td>{{$sis->formula->name}}</td>
                        <td>{{$sis->registrants_count}}</td>
                        <td>
                            @if (!$sis->start_at)
                             <span class="badge badge-danger">BELUM DIBUKA</span> 
                            @elseif(!$sis->end_at)
                             <span class="badge badge-warning">DIBUKA</span> 
                            @elseif(!$sis->announce_at)
                             <span class="badge badge-info">BELUM DIUMUMKAN</span> 
                            @else
                             <span class="badge badge-success">SELESAI</span> 
                            @endif
                        </td>
                        <td class="text-center">
                            @if(!$sis->start_at)
                            <form id="edit-{{$sis->id}}" action="{{ route('internships.update', $sis->id) }}" method="post" style="display: inline;">
                                @method('PATCH')
                                @csrf
                                <input type="hidden" name="status" value="start">
                                <button onClick="return confirm('Apakah Anda yakin ingin membuka lowongan ini?')" class="btn btn-success"><i class="fa fa-check"></i>Mulai</button>
                            </form>
                            @elseif(!$sis->end_at)
                            <form id="edit-{{$sis->id}}" action="{{ route('internships.update', $sis->id) }}" method="post" style="display: inline;">
                                @method('PATCH')
                                @csrf
                                <input type="hidden" name="status" value="end">
                                <button onClick="return confirm('Apakah Anda yakin ingin mengakhiri lowongan ini?')" class="btn btn-warning"><i class="fa fa-times"></i>Akhiri</button>
                            </form>
                            @else
                            <a href="{{ route('internships.results.index', $sis->id) }}">
                                <button class="btn btn-info">Lihat Hasil</button>
                            </a>
                            @endif
                            <a href="{{ route('internships.edit', $sis->id) }}">
                                <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-pencil"></i></button>
                            </a>
                            <form id="delete-siseria-{{$sis->id}}" action="{{ route('internships.destroy', $sis->id) }}" method="post" style="display: inline;">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
<script type="text/javascript">
$(document).ready(function(){
    $("#data-admin_length").append('<a  href="{{ route('internships.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
});
</script>
@endsection
