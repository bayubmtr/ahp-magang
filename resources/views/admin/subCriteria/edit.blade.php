@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">EDIT KRITERIA</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Kriteria / Edit</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('sub-criterias.update', $subCriteria->id) }}">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Formula</label>
                        <input type="text" class="form-control" value="{{ $subCriteria->criteria->formula->name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Kriteria</label>
                        <input type="text" class="form-control" value="{{ $subCriteria->criteria->name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Kriteria</label>
                        <input type="text" class="form-control" name="name" value="{{old('name', $subCriteria->name)}}" id="exampleInputPassword1" placeholder="nama">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Parameter</label>
                        <input type="text" class="form-control" name="parameter" value="{{old('parameter', $subCriteria->parameter)}}" placeholder="Contoh: 0 - 30 / 31 - 60 / 61 - 100">
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
