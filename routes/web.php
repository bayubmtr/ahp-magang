<?php
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\InternshipController;
use App\Http\Controllers\Admin\ResultController as AdminResultController;
use App\Http\Controllers\Admin\FormulaController;
use App\Http\Controllers\Admin\AlternativeController;
use App\Http\Controllers\Admin\CriteriaController;
use App\Http\Controllers\Admin\CriteriaValueController;
use App\Http\Controllers\Admin\SubCriteriaController;
use App\Http\Controllers\Admin\SubCriteriaValueController;

use App\Http\Controllers\User\InternshipVacancyController;
use App\Http\Controllers\User\ResultController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'auth'], function()
{
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('account', AccountController::class);
    Route::resource('users', UserController::class);
    Route::resource('formulas', FormulaController::class);
    Route::resource('internships', InternshipController::class);
    Route::resource('internships.results', AdminResultController::class);
    Route::post('internships/{internship}/results', [AdminResultController::class, 'changeStatus'])->name('internships.results.changeStatus');
    Route::post('internships/{internship}/announce', [AdminResultController::class, 'announce'])->name('internships.results.announce');

    Route::resource('formulas.criterias', CriteriaController::class)->shallow();
    Route::resource('formulas.criteria-values', CriteriaValueController::class)->shallow();
    Route::resource('formulas.criterias.sub-criterias', SubCriteriaController::class)->shallow();
    Route::resource('formulas.criterias.sub-criteria-values', SubCriteriaValueController::class)->shallow();
    Route::resource('alternatives', AlternativeController::class);
    Route::resource('alternative-values', AlternativeValueController::class);

    Route::get('internship-vacancies/{internshipVacancy}/register', [InternshipVacancyController::class, 'registerForm'])->name('internship-vacancies.register');
    Route::post('internship-vacancies/{internshipVacancy}/register', [InternshipVacancyController::class, 'register'])->name('internship-vacancies.register.store');
    Route::resource('internship-vacancies', InternshipVacancyController::class);
    Route::resource('results', ResultController::class);
});
