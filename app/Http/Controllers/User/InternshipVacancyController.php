<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Internship;
use App\Models\Criteria;
use App\Models\Alternative;
use Illuminate\Http\Request;

class InternshipVacancyController extends Controller
{
    public function index()
    {
        $data = Internship::withCount('registrants', 'hasRegister')->latest()
            ->whereNotNull('start_at')
            ->whereNull('end_at')
            ->get();

        return view('user.internship.index', compact('data'));
    }
    
    public function show(Internship $internshipVacancy)
    {
        return view('user.internship.show', compact('internshipVacancy'));
    }
    
    public function registerForm(Internship $internshipVacancy)
    {
        $internshipVacancy = $internshipVacancy->load('formula.criterias.subCriterias');
        return view('user.internship.register', compact('internshipVacancy'));
    }
    
    public function register(Request $request, Internship $internshipVacancy)
    {
        $alternative = Alternative::firstOrCreate([
            'user_id' => auth()->user()->id,
            'internship_id' => $internshipVacancy->id],[
            'status' => 'DAFTAR'
        ]);
        foreach ($request->value as $key => $value) {
            $alternative->alternativeValues()->create([
                'criteria_id' => $key,
                'sub_criteria_id' => $value
            ]);
        }

        return redirect()->route('results.index')->with('success',
        'Berhasil melamar magang');
    }

    public function result()
    {
        $data = Alternative::with('user', 'internship')
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('user.internship.result.index', compact('data'));
    }

}
