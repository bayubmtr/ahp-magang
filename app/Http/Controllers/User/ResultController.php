<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Internship;
use App\Models\Alternative;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function index()
    {
        $data = Alternative::with('user', 'internship')
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('user.internship.result.index', compact('data'));
    }
    public function show($id)
    {
        $data = Internship::with('registrants.user')
            ->whereHas('registrants', function($q){
                $q->where('user_id', auth()->user()->id);
            })
            ->where('id', $id)
            ->firstOrFail();

        if(!$data->announce_at){
            abort(403);
        }

        return view('user.internship.result.show', compact('data'));
    }
}
