<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Internship;
use App\Models\Alternative;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HasilExport;
use App\Http\Controllers\Controller;
use App\Http\Traits\AhpCalculation;

class ResultController extends Controller
{
    use AhpCalculation;
    public function index(Internship $internship, Request $request)
    {
        $data = $this->resultCalculation($internship);
        if($request->type == 'print'){
            return $this->print($data);
        }
        return view('admin.internship.result.index')
        ->with($data);
    }
    
    public function changeStatus(Internship $internship, Request $request)
    {
        if(!$internship->announce_at){
            foreach ($request->alternatives as $key => $value) {
                $alternative = $internship->registrants()->where('id', $value)->update(['status' => $request->status]);
            }
        }
        
        return redirect()->route('internships.results.index', $internship->id)->with('success', 'Berhasil mengubah status');
    }
    
    public function announce(Internship $internship)
    {
        $internship->announce_at = now();
        $internship->save();
        
        return redirect()->route('internships.results.index', $internship->id)->with('success', 'Berhasil mengumumkan hasil');
    }
    
    private function print($data)
    {
        return (new HasilExport ($data))->download('hasil-perangkingan.xlsx');
    }
}
