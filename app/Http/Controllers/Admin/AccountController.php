<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use Redirect;

class AccountController extends Controller
{
    public function index()
    {
        $data = auth()->user();
        return view('account.index')->with('data', $data);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:users,email,'.auth()->user()->id,
            'password' => 'nullable|string|min:5',
            'password_confirmation' => 'nullable|same:password',
         ]);

        $akun = auth()->user();
        if($request->password){
            $akun->password = bcrypt($request->password);
        }
        $akun->name = $request->name;
        $akun->email = $request->email;
        $akun->save();
        return redirect()->route('account.index')->with('success',
        'Berhasil mengubah profil');
    }
}
