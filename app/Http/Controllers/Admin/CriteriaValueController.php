<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Criteria;
use App\Models\CriteriaValue;
use App\Models\Formula;
use Illuminate\Http\Request;
use App\Http\Traits\AhpCalculation;

class CriteriaValueController extends Controller
{
    use AhpCalculation;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Formula $formula)
    {
        $data = $this->criteriaValue($formula);
        return view('admin.criteriaValue.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_kriteria_1' => 'required|exists:criterias,id',
            'id_kriteria_2' => 'required|exists:criterias,id',
            'nilai' => 'required',
         ]);

        if ($request->id_kriteria_1 != $request->id_kriteria_2) {
            $data = CriteriaValue::updateOrCreate(
                [
                    'criteria_1_id' => request('id_kriteria_1'),
                    'criteria_2_id' => request('id_kriteria_2')
                ],
                ['value' => request('nilai')]
            );
            $data = CriteriaValue::updateOrCreate(
                [
                    'criteria_1_id' => request('id_kriteria_2'),
                    'criteria_2_id' => request('id_kriteria_1')
                ],
                ['value' => 1/request('nilai')]
            );

        }
        return redirect()->route('formulas.criteria-values.index', $request->route('formula'))->with('success',
        'Berhasil mengubah bobot kriteria');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CriteriaValue  $criteriaValue
     * @return \Illuminate\Http\Response
     */
    public function show(CriteriaValue $criteriaValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CriteriaValue  $criteriaValue
     * @return \Illuminate\Http\Response
     */
    public function edit($criteriaIds)
    {
        $data = explode(',', $criteriaIds);
        if(isset($data[0]) && isset($data[1])){
            $data1 = Criteria::findOrFail($data[0]);
            $data2 = Criteria::findOrFail($data[1]);
            return view('admin.criteriaValue.edit', compact('data1', 'data2'));
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CriteriaValue  $criteriaValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CriteriaValue $criteriaValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CriteriaValue  $criteriaValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(CriteriaValue $criteriaValue)
    {
        //
    }
}
