<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SubCriteriaValue;
use App\Models\SubCriteria;
use App\Models\Criteria;
use App\Models\Formula;
use Illuminate\Http\Request;
use App\Http\Traits\AhpCalculation;

class SubCriteriaValueController extends Controller
{
    use AhpCalculation;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Formula $formula, Criteria $criteria)
    {
        $data = $this->subCriteriaValue($criteria);
        return view('admin.subCriteriaValue.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_kriteria_1' => 'required|exists:sub_criterias,id',
            'id_kriteria_2' => 'required|exists:sub_criterias,id',
            'nilai' => 'required',
         ]);

        if ($request->id_kriteria_1 != $request->id_kriteria_2) {
            $data = SubCriteriaValue::updateOrCreate(
                [
                    'sub_criteria_1_id' => request('id_kriteria_1'),
                    'sub_criteria_2_id' => request('id_kriteria_2')
                ],
                ['value' => request('nilai')]
            );
            $data = SubCriteriaValue::updateOrCreate(
                [
                    'sub_criteria_1_id' => request('id_kriteria_2'),
                    'sub_criteria_2_id' => request('id_kriteria_1')
                ],
                ['value' => 1/request('nilai')]
            );

        }
        return redirect()->route('formulas.criterias.sub-criteria-values.index', [
            'formula' => $request->route('formula'),
            'criteria' => $request->route('criteria')
        ])->with('success', 'Berhasil mengubah bobot kriteria');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCriteriaValue  $subCriteriaValue
     * @return \Illuminate\Http\Response
     */
    public function show(SubCriteriaValue $subCriteriaValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubCriteriaValue  $subCriteriaValue
     * @return \Illuminate\Http\Response
     */
    public function edit($criteriaIds)
    {
        $data = explode(',', $criteriaIds);
        if(isset($data[0]) && isset($data[1])){
            $data1 = SubCriteria::findOrFail($data[0]);
            $data2 = SubCriteria::findOrFail($data[1]);
            return view('admin.subCriteriaValue.edit', compact('data1', 'data2'));
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubCriteriaValue  $subCriteriaValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCriteriaValue $subCriteriaValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCriteriaValue  $subCriteriaValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCriteriaValue $subCriteriaValue)
    {
        //
    }
}
