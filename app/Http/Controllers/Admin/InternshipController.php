<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Internship;
use App\Models\Formula;
use Illuminate\Http\Request;

class InternshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Internship::withCount('registrants')->latest()->get();

        return view('admin.internship.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formulas = Formula::get();
        return view('admin.internship.create', compact('formulas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string',
            'formula_id' => 'required|exists:formulas,id',
            'description' => 'required|string'
        ]);

        Internship::create($input);

        return redirect()->route('internships.index')->with('success', 'Berhasil menambah data magang');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Internship  $internship
     * @return \Illuminate\Http\Response
     */
    public function show(Internship $internship)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Internship  $internship
     * @return \Illuminate\Http\Response
     */
    public function edit(Internship $internship)
    {
        $formulas = Formula::get();
        return view('admin.internship.edit', compact('formulas', 'internship'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Internship  $internship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Internship $internship)
    {
        if($request->has('status')){
            if($request->status == 'start'){
                $internship->start_at = now();
                $internship->save();
                return redirect()->route('internships.index')->with('success', 'Berhasil membuka magang');
            }elseif($internship->start_at){
                $internship->end_at = now();
                $internship->save();
                return redirect()->route('internships.index')->with('success', 'Berhasil mengakhiri magang');
            }
        }else{
            $input = $request->validate([
                'name' => 'required|string',
                'formula_id' => 'required|exists:formulas,id',
                'description' => 'required|string'
            ]);
            $internship->update($input);
        }

        return redirect()->route('internships.index')->with('success', 'Berhasil mengubah data magang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Internship  $internship
     * @return \Illuminate\Http\Response
     */
    public function destroy(Internship $internship)
    {
        //
    }
}
