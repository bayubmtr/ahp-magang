<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Criteria;
use App\Models\Formula;
use Illuminate\Http\Request;

class CriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $formula)
    {
        $data = Criteria::where('formula_id', $formula)->get();

        return view('admin.criteria.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Formula $formula)
    {
        return view('admin.criteria.create', compact('formula'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Formula $formula)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50'
        ]);

        $formula->criterias()->create($input);

        return redirect()->route('formulas.criterias.index', $formula->id)->with('success', 'Berhasil menambah data kriteria formula '.$formula->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function show(Criteria $criteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function edit(Criteria $criteria)
    {
        return view('admin.criteria.edit', compact('criteria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Criteria $criteria)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50'
        ]);

        $criteria->update($input);

        return redirect()->route('formulas.criterias.index', $criteria->formula_id)->with('success', 'Berhasil mengubah data kriteria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Criteria $criteria)
    {
        $criteria->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus data kriteria');
    }
}
