<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Formula;
use App\Models\Criteria;
use App\Models\SubCriteria;
use Illuminate\Http\Request;

class SubCriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Formula $formula, Criteria $criteria)
    {
        $data = $criteria->subCriterias;

        return view('admin.subCriteria.index', compact('data', 'formula', 'criteria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Formula $formula, Criteria $criteria)
    {
        return view('admin.subCriteria.create', compact('formula', 'criteria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Formula $formula, Criteria $criteria)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50',
            'parameter' => 'required|string|max:100',
        ]);

        $criteria->subCriterias()->create($input);

        return redirect()->route('formulas.criterias.sub-criterias.index', [
            'formula' => $formula->id,
            'criteria' => $criteria->id
        ])->with('success', 'Berhasil menambah data sub kriteria pada kriteria '.$criteria->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCriteria  $subCriteria
     * @return \Illuminate\Http\Response
     */
    public function show(SubCriteria $subCriteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubCriteria  $subCriteria
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCriteria $subCriteria)
    {
        return view('admin.subCriteria.edit', compact('subCriteria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubCriteria  $subCriteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCriteria $subCriteria)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50',
            'parameter' => 'required|string|max:100',
        ]);

        $subCriteria->update($input);

        return redirect()->route('formulas.criterias.sub-criterias.index', ['formula' => $subCriteria->criteria->formula_id, 'criteria' => $subCriteria->criteria_id])->with('success', 'Berhasil mengubah data sub kriteria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCriteria  $subCriteria
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCriteria $subCriteria)
    {
        $subCriteria->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus data sub kriteria');
    }
}
