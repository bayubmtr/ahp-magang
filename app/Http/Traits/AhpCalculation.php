<?php
namespace App\Http\Traits;
use App\Models\Formula;
use App\Models\Criteria;
use App\Models\CriteriaValue;
use App\Models\SubCriteria;
use App\Models\SubCriteriaValue;
use App\Models\AlternativeValue;

trait AhpCalculation {

    public function criteriaValue(Formula $formula)
    {
        $criterias = $formula->criterias()->pluck('name', 'id')->toArray();
        $criteriaIds = $formula->criterias()->pluck('id')->toArray();
        $criteriaValues = CriteriaValue::whereIn('criteria_1_id', $criteriaIds)
            ->orWhereIn('criteria_2_id', $criteriaIds)
            ->get();

        return $this->ahpCalculation($criterias, $criteriaValues);
    }

    public function subCriteriaValue(Criteria $criteria)
    {
        $subCriterias = $criteria->subCriterias()->pluck('name', 'id')->toArray();
        $subCriteriaIds = $criteria->subCriterias()->pluck('id')->toArray();
        $subCriteriaValues = SubCriteriaValue::whereIn('sub_criteria_1_id', $subCriteriaIds)
            ->orWhereIn('sub_criteria_2_id', $subCriteriaIds)
            ->get();

        return $this->ahpCalculation($subCriterias, $subCriteriaValues);
    }

    public function ahpCalculation($kriteria, $bobot)
    {
        //matrix 1 MATRIK PERBANDINGAN BERPASANGAN
        $matrix1 = $this->matrixLangkah1($kriteria,$bobot);
        $matrix2 = [];
        $matrix3 = [];
        $langkah4 = [];
        $langkah5 = [];

        if($matrix1['lengkap'] == true && !$bobot->isEmpty()){
            //Pembuatan matrix 2 nilai kriteria
            $matrix2 = $this->matrixLangkah2($matrix1);

            //Pembuatan matrix 3 penjumlahan setiap baris
            $matrix3 = $this->matrixLangkah3($matrix2['prioritas'], $matrix1['data'], $matrix2['header']);

            //Pembuatan matrix 4 perhitungan rasio konsistensi
            $langkah4 = $this->langkah4($matrix2['prioritas'], $matrix3['jumlah'], $matrix3['header']);

            //Kesimpulan
            $langkah5 = $this->langkah5($langkah4['data'], $langkah4['header']);
        }
        $response = [
                    'skala_kepentingan' => matrikSkala('all'),
                    'matrix1'           => $matrix1,
                    'matrix2'           => $matrix2,
                    'matrix3'           => $matrix3,
                    'langkah4'          => $langkah4,
                    'langkah5'          => $langkah5,
                ];
        return $response;
    }

    public function matrikSkala($n)
    {
        $m_skala = [];
        $m_skala[1] = 0;
        $m_skala[2] = 0;
        $m_skala[3] = 0.58;
        $m_skala[4] = 0.9;
        $m_skala[5] = 1.12;
        $m_skala[6] = 1.24;
        $m_skala[7] = 1.32;
        $m_skala[8] = 1.41;
        $m_skala[9] = 1.46;
        $m_skala[10] = 1.49;
        $m_skala[11] = 1.51;
        $m_skala[12] = 1.48;
        $m_skala[13] = 1.56;
        $m_skala[14] = 1.57;
        $m_skala[15] = 1.59;
        if($n == 'all'){
            return $m_skala;
        }else if(!$n){
            return null;
        }
        return $m_skala[$n];
    }

    public function matrixLangkah1($kriteria,$bobot)
    {
        $matrix1 = [];
        $data = [];
        $jumlah = [];
        $lengkap = false;
        foreach ($kriteria as $keyKriteria1 => $kriteria1) {
            foreach ($kriteria as $keyKriteria2 => $kriteria2) {
                if ($kriteria1 == $kriteria2) {
                    $val = 1;
                } else {
                    foreach ($bobot as $key => $value) {
                        if ($value->criteria_1_id) {
                            if ($value->criteria_1_id == $keyKriteria1 && $value->criteria_2_id == $keyKriteria2) {
                                $val = $value->value;
                            }
                        }
                        elseif ($value->sub_criteria_1_id) {
                            if ($value->sub_criteria_1_id == $keyKriteria1 && $value->sub_criteria_2_id == $keyKriteria2) {
                                $val = $value->value;
                            }
                        }
                    }
                }
                $data[$keyKriteria1][$keyKriteria2]=$val;
                $val = null;
            }
        }

        $jumlah = array();

        array_walk_recursive($data, function($item, $key) use (&$jumlah){
            $jumlah[$key] = isset($jumlah[$key]) ?  $item + $jumlah[$key] : $item;
        });

        if(!$bobot->isEmpty() && !empty($kriteria)){
            if((count($bobot) + count($kriteria)) / count($kriteria) == count($kriteria)){
                $lengkap = true;
            }
        }

        $matrix1['header'] = $kriteria;
        $matrix1['data'] = $data;
        $matrix1['jumlah'] = $jumlah;
        $matrix1['lengkap'] = $lengkap;

        return $matrix1;
    }

    public function matrixLangkah2($matrix1)
    {
        $matrix2 = [];
        foreach ($matrix1['data'] as $key => $value) {
            foreach ($value as $key2 => $value2) {
                //nilai kolom dibagi jumlah kolom
                $data[$key][$key2] = $value2 / $matrix1['jumlah'][$key2];
            }
            //jumlah baris
            $jumlah[$key] = array_sum($data[$key]);
            //jumlah dibagi total kolom
            $prioritas[$key] = array_sum($data[$key])/count($data[$key]);
        }

        foreach ($prioritas as $key => $value) {
            $prioritas_subkriteria[$key] = $value / max($prioritas);
            $kumpulan_prioritas[$key] = $prioritas_subkriteria[$key];
        }

        $matrix2['header'] = $matrix1['header'];
        $matrix2['data'] = $data;
        $matrix2['jumlah'] = $jumlah;
        $matrix2['prioritas'] = $prioritas;
        $matrix2['prioritas_subkriteria'] = $prioritas_subkriteria;
        $matrix2['kumpulan_prioritas'] = $kumpulan_prioritas;

        return $matrix2;
    }

    public function matrixLangkah3($prioritasMatrix2,$matrix1, $header)
    {
        $matrix3 = [];
        foreach ($matrix1 as $key => $value) {
            foreach ($value as $key2 => $value2) {
                //nilai prioritas matrix 2 dikali nilai matrix 1
                $data[$key][$key2] = $prioritasMatrix2[$key2] * $value2;
            }
            //jumlah baris
            $jumlah[$key] = array_sum($data[$key]);
        }
        $matrix3['header'] = $header;
        $matrix3['data'] = $data;
        $matrix3['jumlah'] = $jumlah;

        return $matrix3;
    }

    public function langkah4($bobotMatrix2, $jumlahMatrix3, $header)
    {
        for ($x = 0; $x < 2; $x++) {
            foreach ($bobotMatrix2 as $key => $value) {
                if($x == 0){
                    //jumlah per baris pada matrix 3
                    $data[$key]['jumlah'] = $jumlahMatrix3[$key];
                }else{
                    //prioritas pada matrix 2
                    $data[$key]['prioritas'] = $value;
                    //prioritas + jumlah
                    $data[$key]['hasil'] = $value + $data[$key]['jumlah'];
                }
            }
        }
        $langkah4['header'] = $header;
        $langkah4['data'] = $data;

        return $langkah4;;
    }

    public function langkah5($langkah4, $header)
    {
        $langkah5['jumlah'] = 0;
        foreach ($langkah4 as $key => $value) {
            $langkah5['jumlah'] += $langkah4[$key]['hasil'];
        }
        $langkah5['n'] = count($header);
        $langkah5['lamda'] = $langkah5['jumlah'] / $langkah5['n'];
        $langkah5['ci'] = ($langkah5['lamda']-$langkah5['n'])/$langkah5['n'];
        $langkah5['cr'] = $this->matrikSkala($langkah5['n']) > 0 ? $langkah5['ci']/$this->matrikSkala($langkah5['n']) : 0;
        if($langkah5['cr'] <= 0.1 ){
            $langkah5['konsisten'] = 'Konsisten';
        }else{
            $langkah5['konsisten'] = 'Tidak Konsisten';
        }

        return $langkah5;
    }

    public function resultCalculation($internship)
    {
        $alternatif = $internship->registrants->load('user');
        $bobotKriteria = $this->criteriaValue($internship->formula);
        $matrixHasil = [];
        $nilaiAlternatif = [];
        $prioritasSubKriteria = [];
        $prioritasKriteria = [];
        $nilaiAlternatif = [];
        $subKriteria = [];

        if($bobotKriteria['matrix1']['lengkap']){

            $matrixHasil['header'] = $bobotKriteria['matrix1']['header'];
            $matrixHasil['prioritas'] = $bobotKriteria['matrix2']['prioritas'];
            $prioritasKriteria = $bobotKriteria['matrix2']['prioritas'];
            $nilaiAlternatif['header'] = $bobotKriteria['matrix1']['header'];

            $x = 0;
            foreach ($matrixHasil['header'] as $key => $value) {
                $criteria = Criteria::find($key);
                $bobotSubKkriteria = $this->subCriteriaValue($criteria);
                $prioritasSubKriteria += $bobotSubKkriteria['matrix2']['prioritas_subkriteria'];
                $subKriteria += $bobotSubKkriteria['matrix1']['header'];
                $i=0;
                foreach ($bobotSubKkriteria['matrix2']['prioritas_subkriteria'] as $key2 => $value2) {
                    $matrixHasil['data'][$x]['prioritas'][$i] = $value2;
                    $matrixHasil['data'][$x]['header'][$i] = $bobotSubKkriteria['matrix1']['header'][$key2];
                    $i++;
                }
                $x++;
            }
            foreach ($alternatif as $key => $value) {
                $total = 0;
                foreach ($matrixHasil['header'] as $keyHeader => $valueHeader) {
                    $nilaiAlternatifDB = AlternativeValue::whereHas('subCriteria', function($q) use($keyHeader){
                        $q->where('criteria_id', $keyHeader);
                    })->where('alternative_id', $value->id)->first()->toArray();
                    $nilaiAlternatif['data'][$value->id]['id'] = $value->id;
                    $nilaiAlternatif['data'][$value->id]['nama'] = $value->user->name;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader] = $nilaiAlternatifDB;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader]['nama'] = $subKriteria[$nilaiAlternatifDB['sub_criteria_id']];
                    $prioritas_kriteria = $matrixHasil['prioritas'][$keyHeader];
                    $prioritas_subkriteria = $prioritasSubKriteria[$nilaiAlternatifDB['sub_criteria_id']];
                    $hasil = $prioritas_kriteria * $prioritas_subkriteria;
                    $total += $hasil;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader]['hasil'] = $hasil;
                    $nilaiAlternatif['data'][$value->id]['total'] = $total;
                    $nilaiAlternatif['data'][$value->id]['status'] = $value->status;
                }
            }
            $rangking = array_column($nilaiAlternatif['data'], 'total');
            array_multisort($rangking, SORT_DESC, $nilaiAlternatif['data']);
        }

        return ['matrixHasil' => $matrixHasil,
                'nilaiAlternatif' => $nilaiAlternatif
            ];
    }
}