<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'formula_id',
        'created_at',
        'updated_at'
    ];

    public function formula()
    {
        return $this->belongsTo('App\Models\Formula', 'formula_id', 'id');
    }

    public function subCriterias()
    {
        return $this->hasMany('App\Models\SubCriteria', 'criteria_id', 'id');
    }
}
