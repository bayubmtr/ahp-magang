<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Internship extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'description',
        'start_at',
        'end_at',
        'announce_at',
        'formula_id',
        'created_at',
        'updated_at'
    ];

    public function registrants()
    {
        return $this->hasMany('App\Models\Alternative', 'internship_id', 'id');
    }

    public function hasRegister()
    {
        return $this->hasOne('App\Models\Alternative', 'internship_id', 'id')
            ->where('user_id', auth()->user()->id);
    }

    public function formula()
    {
        return $this->belongsTo('App\Models\Formula', 'formula_id', 'id');
    }
}
