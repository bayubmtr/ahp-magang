<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formula extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at'
    ];

    public function internships()
    {
        return $this->hasMany('App\Models\Internship', 'formula_id', 'id');
    }

    public function criterias()
    {
        return $this->hasMany('App\Models\Criteria', 'formula_id', 'id');
    }
}
