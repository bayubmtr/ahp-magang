<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCriteriaValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'sub_criteria_1_id',
        'sub_criteria_2_id',
        'value',
        'created_at',
        'updated_at'
    ];


    public function firstSubCriteria()
    {
        return $this->belongsTo('App\Models\SubCriteria', 'sub_criteria_1_id', 'id');
    }
    public function secondSubCriteria()
    {
        return $this->belongsTo('App\Models\SubCriteria', 'sub_criteria_2_id', 'id');
    }
}
