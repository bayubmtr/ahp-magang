<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlternativeValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'alternative_id',
        'criteria_id',
        'sub_criteria_id',
        'created_at',
        'updated_at'
    ];

    public function subCriteria()
    {
        return $this->belongsTo('App\Models\SubCriteria', 'sub_criteria_id', 'id');
    }
    public function criteria()
    {
        return $this->belongsTo('App\Models\Criteria', 'criteria_id', 'id');
    }
}
