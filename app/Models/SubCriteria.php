<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCriteria extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'parameter',
        'criteria_id',
        'created_at',
        'updated_at'
    ];

    public function criteria()
    {
        return $this->belongsTo('App\Models\Criteria', 'criteria_id', 'id');
    }
}
