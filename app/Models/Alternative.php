<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'user_id',
        'internship_id',
        'status',
        'created_at',
        'updated_at'
    ];

    public function alternativeValues()
    {
        return $this->hasMany('App\Models\AlternativeValue', 'alternative_id', 'id');
    }
    public function internship()
    {
        return $this->belongsTo('App\Models\Internship', 'internship_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
