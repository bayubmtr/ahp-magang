<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CriteriaValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'criteria_1_id',
        'criteria_2_id',
        'value',
        'created_at',
        'updated_at'
    ];

}
